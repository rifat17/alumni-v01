import React, { useReducer } from "react";
import { Auth } from "aws-amplify";
import { Redirect } from "react-router-dom";

const initState = {
  username: "",
  email: "",
  password: "",
  confirm_password: "",
  isLoading: false,
  error: "",
};
function registerReducer(state, action) {
  switch (action.type) {
    case "field":
      return {
        ...state,
        [action.field]: action.value,
      };
    case "signup":
      return {
        ...state,
        isLoading: true,
      };
    case "success":
      return {
        ...state,
        username: "",
        password: "",
        email: "",
        isLoading: false,
        error: "",
      };
    case "error":
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

function Register(props) {
  const [state, dispatch] = useReducer(registerReducer, initState);
  const { username, email, password, confirm_password, isLoading, error } =
    state;
  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch({
      type: "signup",
    });

    const user = {
      username: username,
      email,
      password: password,
      confirm_password: confirm_password,
    };
    console.log(user);

    try {
      const response = await Auth.signUp({
        username,
        password,
        attributes: {
          email: email,
        },
      });
      console.log(response);
      dispatch({
        type: "success",
      });
      <Redirect to="/welcome" />;
    } catch (error) {
      console.log(error);
      dispatch({
        type: "error",
        payload: "some error",
      });
    }
  };

  return (
    <div className="text-center">
      <h1>User Signup</h1>
      <form
        onSubmit={handleSubmit}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div className="form-group">
          <label>Username</label>
          <input
            className="form-control"
            type="text"
            name="username"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "username",
                value: e.target.value,
              })
            }
          />
          <label>Email</label>
          <input
            className="form-control"
            type="text"
            name="email"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "email",
                value: e.target.value,
              })
            }
          />
          <label>Password</label>
          <input
            className="form-control"
            type="password"
            name="password"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "password",
                value: e.target.value,
              })
            }
          />
          <label>Confirm Password</label>
          <input
            className="form-control"
            type="password"
            name="password"
            required={true}
            onChange={(e) =>
              dispatch({
                type: "field",
                field: "confirm_password",
                value: e.target.value,
              })
            }
          />
          <span style={{ color: "red" }} className="warn">
            {error}
          </span>
          <button
            type="submit"
            disabled={isLoading}
            className="btn btn-primary"
          >
            {isLoading ? "Signing Up.." : "SignUp"}
          </button>
        </div>
      </form>
    </div>
  );
}

export default Register;

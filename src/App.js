import logo from "./logo.svg";
import "./App.css";
import Register from "./components/Auth/Register";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Register />
      </header>
    </div>
  );
}

export default App;
